# flask_redis docker demo

## Running the flask app locally (0.1.0)

Install the requirements in a virtual environment:

```
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```

Run the application:

```
export FLASK_APP=app.py
flask run
```

Go to http://127.0.0.1:5000

## Running the flask app with docker (0.2.0)

Build the docker image:

```
docker build -t flask_redis .
```

Run the application:

```
docker run -d -p 5000:5000 flask_redis
```

## Connect to Redis (0.3.0)

Start Redis container:

```
docker run --name my-redis -d redis
```

Connect to redis from the flask app:

```
docker run --link my-redis:redis -d -p 5000:5000 flask_redis
```

## Running the flask app with docker-compose (0.4.0)

Run the application:

```
docker-compose up
```

Note that it will automatically build the flask_redis image if not present. Try it.

```
docker-compose stop
docker-compose rm -f
docker rmi flask_redis
docker-compose up
```

What happens when you remove and restart the redis container?

## Adding persistent storage (0.5.0)

Check what was added in the docker-compose.yml file.

```
git diff 0.4.0 -- docker-compose.yml
```

Run the application:

```
docker-compose up -d
```

Remove and restart the containers. What happens?

```
docker-compose stop
docker-compose rm -f
docker-compose up
```
