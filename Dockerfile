FROM python:3.6-slim

COPY requirements.txt /requirements.txt

RUN python -m venv /venv \
  && . /venv/bin/activate \
  && pip install --no-cache-dir -r /requirements.txt

RUN groupadd -r -g 1000 csi \
  && useradd --no-log-init -r -g csi -u 1000 csi

COPY --chown=csi:csi . /app/
WORKDIR /app

ENV PATH /venv/bin:$PATH
ENV FLASK_APP app.py

EXPOSE 5000

USER csi

CMD ["flask", "run", "--host", "0.0.0.0"]
